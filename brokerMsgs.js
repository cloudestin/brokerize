﻿function initQamoos(req){
	var qamoos = require("qamoos")(req);	
	var book = qamoos.define("brokerMsgs");

	book.set('serverRunning', {"fr":"Serveur a l'ecoute.." , en:"Server listening to ports.." });
	book.set('appName', {"fr":"Nom de l'appli" , en:"appName" });
	book.set('defaut', {fr:"defaut" , en:"default"});

	book.set('ok', {fr:'ok'}, 0);

	book.set('pendentLogin', {fr:"Tentative en cours",en:"Processing login.."},-102);
	book.set('pendentSess', {fr:"Session en cours",en:"Pendent session.."},-130);
	book.set('expiredAccount', {fr:"Abonnement expiré" , en:"Expired account"},-106);
	book.set('networkProblem', {fr:"Problème réseau" , en:"Network problem"},-107);
	book.set('badLogin', {fr:"Login incorrect", en:"Bad login"},-103);
	book.set('badPassword', {fr:"Mot de passe incorrect", en:"Bad password"},-104);
	
	book.set('inactiveAccount', {fr:"Compte inactif", en:"Account blocked."},-105);
	book.set('badOldPass', {fr:"Mot de passe courant incorrect.", en:"Incorrect current password."},-110);
	book.set('unsecurePass', {fr:"Mot de passe trop court ou pas assez sécurisé.", en:"Unsecure password."},-111);
	book.set('samePass', {fr:"Mots de passe identiques.", en:"Password unchanged."},-112);
	book.set('DBUpdateError', {fr:"Erreur de mise-à-jour de la base de données.", en:"Database update error."},-113);
	book.set('missedData', {fr:"Données manquantes" , en:"Data missing"},-114);
	book.set('badData', {fr:"Données incorrectes.",en:"Bad data."},-115);
	book.set('dataAccessErr', {fr:"Erreur d'acces aux donnees" , en:"Data acces error.."},-126);
	book.set('failedCompanyCreation', {fr:"Création de l'entreprise échouée.",en:"Company creation failed."},-117);
	book.set('loginUnavailable', {fr:"Nom d'utilisateur déjà pris.", en:"Login unavailable."},-118);
	book.set('emailUnavailable', {fr:"Adresse email déjà utilisée.", en:"Email unavailable."},-119);
	book.set('companyUnvailable', {fr:"L'entreprise existe déjà.",en:"Company already exists."},-120);
	book.set('failedAccountCreation', {fr:"Création de compte échouée.", en:"Account creation failed."},-121);
	book.set('ipAlreadyUsed', {fr:"Un seul compte Démo autorisé.", en:"You have already created an account."},-122);
	book.set('loginNotFound', {fr:"Login introuvable." , en:"Login not found."},-123);
	book.set('emailNotFound', {fr:"Email introuvable." , en:"Email not found."},-124);
	book.set('errSendEmail', {fr:"Erreur d'envoi du message." , en:"Email send failed."},-125);
	book.set('noSession', {fr:"Pas de session." , en:"No session."},-131);
	return book;
}


module.exports = function(req){
 var book = initQamoos(req);
 return book;
}