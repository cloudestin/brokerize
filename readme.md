# Brokerize #

**brokerize** is a _Node.js_ broker using  MySql databases. A **broker** is composed by a session manager and a data exchange module with the front end. It uses a structure of 3 databases:

* _admin database_ for connection and user credentials. A user belongs to a company and have some rights defined.
* _modele database_ for readbale data to store static data about your application
* _user data_ where is stored user and company data.

## How does it wok ##
You should first activate session management, using '*exress-session*' middleware.

**Brokerize** is to be used as an _express.js_ middleware. The entry point of the broker is the admin database, defined by a **treble** object db3 below.

Then the broker is mounted on a specified path. Admin database is managed by the broker. In the **config** method you should give the path where to find modle and user schemas to access to modele and user data.

Once signed in, the user will have access to a modele database (for reading) and a user database (read/write), depending on his company. Admin database has there tables like *companies*, *databases* and *modeles* to give this information. A user belongs to a company but can have access to an other company data indicated in the *userCompanies* data. 

```javascript
var express = require('express');
var expressSession = require('express-session');
var app = express();
var Brokerize = require("Brokerize");
var Sequelize = require("sequelize");
var db3 = require("treble")(Sequelize,'mysql://user:password@host:port/database');

app.use( expressSession({secret:'mySecretKey', cookie: { maxAge: 600000 }, resave:false, saveUninitialized:true}) );

app.use( "/broker", Brokerize(db3).config({schemas:__dirname+"/schemas/"}) );
```

The broker now can be accessed by using HTTP request:

- POST /broker/signin : opens a session in the broker. credentials are sent as a JSON object in the body of the request. It returns a json object in the form of {err:number, msg:text}
- GET /broker/signout : close the current session
- GET /broker/guard returns the login of the used currently signin or null if no-user connected.
- GET /broker/sess returns an object containing information about the user connected, his company and the companies he can have access to.. 
- POST /broker/chgPw to change the user's password
- GET /lang/[fr|en] : change the broker language, error messages,.. currently french and english supported. fr is the default value 
- POST /broker/schema : to obtain information a table structure in the modele and user databases
- POST /broker/missed : gives a key/hash couple for email reset.These key/hash serve to form an url to send by email that will serve once. 
- POST /broker/reset : reset a user password without being logged (missed password) given (key,hash) values and a new password.

## Data manipulation ##
You schould define schemas for the user data and modele data.
Data manipulation is done using the ```[mountedpath]/d``` url (/broker/d in the example above). The verbs used are GET, POST and DELETE.

### Read data from a table `GET /d` ###

 __Example:__
 
`GET /broker/d?c=company&d=db&s=schema&o=options`

or

`GET /broker/d?s=schema&count=y&o=options`


### Update a record `POST /d` ###

 __Example:__
 
`POST /broker/d {post:{c:company, s:schema, k:key, v:value}}`

### Add a record `POST /d` ###

 __Example:__
 
`POST /broker/d {post:{c:company, s:schema, v:value}}`


### Delete a record `DELETE /d` ###

 __Example:__
 
`DELETE /broker/d {post:{c:company, d:db, s:schema, k:key}}`