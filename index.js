var db3,
	qamoos,
	ID=require("./ID"),
	MD5=require("./md5"),
	Hash = require("./hash"),
	path = require("path"),
	schematize=require("schematize").config({path:{
			admin:path.join(__dirname,'schemas/')
		}}),
	express=require("express"),
	expressSession=require("express-session"),
	uniC = require("unic"),
	bodyParser = require('body-parser'),
	varServer = require("varserver")(),
	data;


function Middleware(db3){
	var middleware = express.Router();

	data = require("./data")(db3,uniC);

	middleware.use( bodyParser.json({extended:true}) );
    middleware.use( bodyParser.urlencoded({extended:true}) );
	middleware.post('/signin' , require("./signin")(db3) );
	middleware.get('/signout' , signout );
	middleware.get('/guard'   , guard );
	middleware.get('/sess'   , getSession );
	middleware.get('/lang/:lang'   , setLang );
	middleware.post('/chgpw'   , chgPw );
	middleware.post('/resetpw'   , resetPw );
	middleware.post('/schema' , getSchema );
	middleware.post('/missed' , missed );
	middleware.post('/reset' , reset ); 
	middleware.route('/d')
		.get( data.getData )
		.post( data.postData )
		.delete( data.deleteData );
	
	middleware.get('/spy' , function(req,res){console.log("spy: ", varServer.get("schematizePath"));} );

	function getSchema(req, res, next) {
		var post= JSON.parse((req.body && req.body.post)||"{}");
		var schema=post.s;
		var db=(post.d && post.d.match(/^(modele|m)$/) ) ? "modele" : "user";

		if (!uniC(req).connected()) {res.status(200).json(null);return;} // no session
		if (!schema) {res.status(200).json(null);return;} // no schema
		schematize( db , schema  ).then(
			function(e){ res.status(200).json(e.jSchema() );},
			function(){ res.status(404).json( null );} );
	}

	function checkPw( pw ){
		pw +="";
		if (pw.length <6) return false;
		if (!pw.match(/[a-zA-Z]/g)) return false;
		if (!pw.match(/[0-9]/g)) return false;
		return true;
	}

	function missed(req,res,next){
	 var post= (req.body && req.body.post)||"{}";
	 var uniC = require("unic")(req);

	 try {
	 	post = JSON.parse( post );
	 }
	 catch(e) {
	 	post = {};
	 }

	var email=post.email,stamp=post.stamp,ts=post.ts;
	if (uniC.connected()) {res.status(200).json(qamoos.getErr('pendentSess'));}
	if (!email || !ts) {res.status(200).json(qamoos.getErr('missedData'));}
	if (MD5(email+ts) != stamp) {res.status(200).json(qamoos.getErr('badData'));}
	if (!qamoos) qamoos = require("./brokerMsgs")(req);
	schematize("admin",'users',db3)
		.error(function(t){res.status(200).json(qamoos.getErr('missedData'));})
		.success(function(users){
			var where;
			switch( ID(email) ) {
				case 'login': where={where:{login:email}};break;
				case 'email': where={where:{email:email}};break;
				default: res.status(200).json(qamoos.getErr('badLogin'));return;
			 }
			users.find(where).success( function(User){
				var user=User(), chars="abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_1234567890~@#*/+";
				if (!user) {res.status(200).json(qamoos.getErr('badLogin'));return;}
				var val={key:"",userID:user.userID};
				while( val.key.length<32) val.key += chars[Math.floor(Math.random()*chars.length)];
				val.hash = Hash( user, val.key );
				res.status(200).json(val);
			});
		});
	}

	function reset(req,res,next){
	 var post= (req.body && req.body.post)||"{}";
	 var uniC = require("unic")(req);

	 if (!qamoos) qamoos = require("./brokerMsgs")(req);

	 try {
	 	post = JSON.parse( post );
	 }
	 catch(e) {
	 	post = {};
	 }

	if (uniC.connected()) {res.status(200).json(qamoos.getErr('pendentSess'));}
	if (!post.email || !post.key || !post.hash) {res.status(200).json(qamoos.getErr('missedData'));return;}
	if (!checkPw(post.pw)) {res.status(200).json( qamoos.getErr('unsecurePass'));return;}

	schematize("admin",['users'],db3)
		.error(function(t){console.log(t);res.status(200).json(qamoos.getErr('missedData'));})
		.success(function(tabs){
			var where;

			switch( ID(post.email) ) {
				case 'login': where={where:{login:post.email}};break;
				case 'email': where={where:{email:post.email}};break;
				default: res.status(200).json(qamoos.getErr('badLogin'));return;
			 }
			tabs.users.find(where).success( function(User){
				var user=User();
				if (!user) {res.status(200).json(qamoos.getErr('badLogin'));return;}
				if (Hash( user, post.key ) != post.hash) {res.status(200).json(qamoos.getErr('badData'));return;}
				tabs.users.update({'password':post.pw},{where:{userID:user.userID}});
				res.status(200).json({});
			});
		});
	}

	function signout(req,res,next){
		res.status(200).json( uniC(req).signout());
	}

	function guard(req,res,next) {
		res.status(200).json( uniC(req).guard() );
	}

	function getSession(req,res,next) {
	 var session=uniC(req).get();
	 res.status(200).json( (session&&session.extr)||null);
	}

	function setLang(req,res,next) {
	 var lang = req.params.lang;
	 if (!qamoos) qamoos = require("./brokerMsgs")(req);
	 res.status(200).json( qamoos.lang(lang) && uniC(req).lang(lang));
	}

	// change password of active session
	function chgPw(req,res,next) {
		var login = uniC(req).getLogin(),
			oldPass,
			newPass,
			timeStamp;

		if (!qamoos) qamoos = require("./brokerMsgs")(req);
		if (!login) {res.status(200).json( qamoos.getErr('networkProblem') );return;}

		var post= (req.body && req.body.post)||"{}";
		try {
			post = JSON.parse( post );
		}
		catch(e) {
			post = {};
		}

		oldPass=post.old;
		newPass=post.new;
		timestamp=post.ts;

		schematize("admin","users",db3).then(
			function( users ) {
			 users.find({where:{login:login}}).success( function(User){
			 var user=User();
			 if (!user) {res.status(200).json( qamoos.getErr('badLogin'));return;}
			 if (!checkPw(newPass)) {res.status(200).json( qamoos.getErr('unsecurePass'));return;}
			 var footprint = user.login+user.password+newPass+timestamp;
			 if (MD5(user.login+user.password+newPass+timestamp) != oldPass) {res.status(200).json(qamoos.getErr('badPassword'));return;}
			 if (user.password == newPass) {res.status(200).json( qamoos.getErr('samePass'));return;}

			 users.update({'password':newPass},{where:{userID:user.userID}})
			 .error(
				function(){
					 res.status(200).json( qamoos.getErr('dataAccessErr'));
					})
			 .success(
				function(){
					 res.status(200).json( qamoos.getErr('ok'));
					});
			});
			},
		function(){res.status(200).json( qamoos.getErr('dataAccessErr'));return;}
		);

	}

	// reset password when not signed in
	function resetPw(req,res,next) {
		var oldPass,
			newPass,
			timeStamp;

		if (!qamoos) qamoos = require("./brokerMsgs")(req);
		if (uniC(req).connected()) {res.status(200).json( qamoos.getErr('networkProblem') );return;}

		var post= (req.body && req.body.post)||"{}";
		try {
			post = JSON.parse( post );
		}
		catch(e) {
			post = {};
		}

		var userID =post.id; 
		var pass=post.pass;
		var key =post.key; 
		var hash =post.hash; 

		middleware.getUser(userID,key,hash)
		.success(function(user){
			schematize("admin","users",db3)
				.success(
					function( users ) {
						 users.update({'password':pass},{where:{userID:userID}})
						 .error(
							function(){
								 res.status(200).json( qamoos.getErr('dataAccessErr'));
								})
						 .success(
							function(){
								 res.status(200).json( qamoos.getErr('ok'));
								});
				})
				.error( function(){res.status(200).json( qamoos.getErr('dataAccessErr'));});
		 })
		.error(function(){ // getUser error
			res.status(200).json( qamoos.getErr('dataAccessErr'));			
		});
	}

	middleware.config=function( options ){
		var path = options.schemas;

		if (path)  schematize.config({path:{modele:path+"/modele/",user:path+"/user/"}});
		return middleware;
	}

	middleware.getUser = function getUser(userID,key,hash){
		var onsuccess=function(){},
			onerror=function(){};

		schematize("admin","users",db3)
			.success(
				function( users ) {
					 users.find({where:{userID:userID}})
					 	.success( function(User){
							 var user=User();
							 if (!user || (hash != Hash( user, key ))) onerror();
							 else onsuccess(user);
							})
					 	.error( onerror );
				})
			.error( function(){console.log("getUser.schematize.error: ");});
		return {
				success:function(e){onsuccess=e;return this;},
				error:function(e){onerror=e;return this;},
				then:function(succ,err){ onsuccess=succ|| onsuccess; onerror=err || onerror; return this;}
			}
	}

	return middleware;
}

module.exports = Middleware;
	