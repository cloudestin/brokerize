module.exports = function(DataTypes) {
  return ['users',{
	 userID:{type:DataTypes.INTEGER, autoIncrement: true, allowNull: false, primaryKey:true},
	 login:{type:DataTypes.STRING(25),  allowNull:false, unique:true
				// currently validation bugs on update
				//,validate: {isLogin: function(value) {if (value && !value.match(/^[a-zA-Z0-9][a-zA-Z0-9\_\-]{3,}$/)) throw new Error('Bad_Login!');}}
			},
	 email:{type:DataTypes.STRING(150), allowNull:true,  unique:true, validate:{isEmail:true}},
	 password:{type:DataTypes.STRING(50),allowNull:false},
	 name:{type:DataTypes.STRING(80),allowNull:false},
	 phone:{type:DataTypes.STRING(50),allowNull:true},
	 inactive:{type:DataTypes.BOOLEAN,allowNull:false, defaultValue:false},
	 idCompany:{type:DataTypes.INTEGER, allowNull:false},
	 securityLevel:{type:DataTypes.ENUM , values:["User" ,"CompanyAdmin","BookKeeper","SuperAdmin"], allowNull:false, defaultValue:"User"},
	 
	 ipCreation:{type:DataTypes.STRING(50),allowNull:true},
	 lastLogin:{type:DataTypes.DATE, allowNull:true},
	 lastAttempt:{type:DataTypes.DATE, allowNull:true}
	},
	{
	 charset: 'utf8',
     collate: 'utf8_general_ci',
	 comment:"Liste globale de tous les users/passwords/societes"
	}];
}