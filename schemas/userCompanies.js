module.exports = function(DataTypes) {
  return ['userCompanies', {
	 login:{type:DataTypes.STRING(50),allowNull:false, primaryKey:true},
	 company:{type:DataTypes.INTEGER , allowNull:false, primaryKey:true}
	},
	{
	 charset: 'utf8',
     collate: 'utf8_general_ci',
	 comment:"Liste des soci�t�s auxqueles un utilisateur peut avoir acc�s (comme: administrateur, gestionnaire)"
	}];
}