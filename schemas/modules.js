module.exports = function(DataTypes) {
  return ['modules', {
	 id:{type:DataTypes.STRING(10),allowNull:false,primaryKey:true},
	 longName:{type:DataTypes.STRING(50),allowNull:false},
	 shortName:{type:DataTypes.STRING(25),allowNull:false},
	 inactive:{type:DataTypes.BOOLEAN,allowNull:false,"default":"0"},
	 minLevel:{type:DataTypes.INTEGER , allowNull:false , "default":"0"},
	 path:{type:DataTypes.STRING(25),allowNull:false},
	 icone:{type:DataTypes.STRING(32),allowNull:true},
	 classement:{type:DataTypes.STRING(10),allowNull:true}
	},
	{
	 charset: 'utf8',
     collate: 'utf8_general_ci',
	 comment:"Liste globale des modules de l'application"
	}];
}