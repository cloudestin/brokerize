module.exports = function(DataTypes) {
  return ['companies', {
	 nom:{type:DataTypes.STRING(30), allowNull:false,unique:true},
	 idDB:{type:DataTypes.INTEGER , allowNull:false},
	 idModele:{type:DataTypes.INTEGER , allowNull:false},
	 demo:{type:DataTypes.BOOLEAN, allowNull:false, defaultValue:false},
	 accessExpiration:{type:DataTypes.DATE, allowNull:true},
	 alertPeriode:{type:DataTypes.INTEGER, allowNull:false, defaultValue:10},
	 redemptionPeriode:{type:DataTypes.INTEGER, allowNull:false, defaultValue:3},
	 ipCreation:{type:DataTypes.STRING(50), allowNull:true}
	},
	{
	 charset: 'utf8',
     collate: 'utf8_general_ci',
	 instanceMethods: {
		 isAlert: function() {return (this.accessExpiration && (this.accessExpiration>getdate()) && (this.accessExpiration<(new Date()+alertPeriode)))},
		 isRedemption: function(){return (this.accessExpiration && ((new Date())>this.accessExpiration) & ((new Date())<(this.accessExpiration+redemptionPeriode)));},
		 isExpiration: function(){return (this.accessExpiration && ((new Date())>(this.accessExpiration+this.redemptionPeriode)) );}
		},
	 comment:"Table de toutes les societes, a chacune est affectee une base modele et une base utilisateur"
	}];
}