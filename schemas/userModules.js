module.exports = function(DataTypes) {
  return ['userModules', {
	 login:{type:DataTypes.STRING(50) , allowNull:false , primaryKey:true },
	 company:{type:DataTypes.INTEGER, allowNull:false , primaryKey:true },
	 module:{type:DataTypes.STRING(10) , allowNull:false , primaryKey:true },
	 inactive:{type:DataTypes.BOOLEAN, allowNull:false , defaultValue:false },
	 expiration:{type:DataTypes.DATE, allowNull:true }
	},
	{
	 charset: 'utf8',
     collate: 'utf8_general_ci',
	}];
}