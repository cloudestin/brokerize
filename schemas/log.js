module.exports = function(DataTypes) {
  return ['log', {
	 id:{type:DataTypes.INTEGER, autoIncrement:true, allowNull:false, primaryKey:true},
	 login:{type:DataTypes.STRING(50),allowNull:true},
	 log:{type:DataTypes.STRING(1024) , allowNull:true},
	 date:{type:DataTypes.DATE , defaultValue:DataTypes.NOW, allowNull:false}
	},
	{
	 comment:"Table utilis�e pour le debuggage"
	}];
}