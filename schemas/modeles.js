module.exports = function(DataTypes) {
  return ['modeles', {
	 id:{type: DataTypes.INTEGER, autoIncrement: true, allowNull: false, primaryKey: true},
	 cs:{type:DataTypes.STRING, allowNull:true},
	 server:{type:DataTypes.STRING(50), allowNull:true},
	 dbName:{type:DataTypes.STRING(50), allowNull:true},
	 user:{type:DataTypes.STRING(50), allowNull:true},
	 password:{type:DataTypes.STRING(50), allowNull:true},
	 demo:{type:DataTypes.BOOLEAN,allowNull:false,defaultValue:false}	 
	},
	{
	 comment:"Liste de toutes les base de donn�es 'modele' utilis�es"
	}];
}