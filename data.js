	/**** GET /d?c=company&d=db&s=schema&o=options ****/
module.exports = function(db3, uniC){

	function getData(req,res){
			 var company=req.query.c,
				 db=req.query.d,
				 schema=req.query.s,
				 options;
				 session=uniC(req).get();

			  try {
			   options = JSON.parse(req.query.o);
			  }
			  catch(error){
			   options = {};
			  }

			  if (req.query.count){ // GET ?count=true  returns the number of records only
				  if (!options.where) options.where={};
				  if (!session) {res.status(200).json(-1);return;}
				  if (typeof company === "undefined") {company = session.intr.c0;} // default company
				  if (!session.intr.c1[company]) {res.status(200).json(-1);return;} // incorrect company
				  if (!schema) {res.status(200).json(-1);return;} // no schema name given

				  switch(db){
				   case "m":
				   case "modele": db="modele"; break;
				   default :
					 db="user"
					 options.where.company = company;
				  }

				  var dbid = session.intr.c1[company];
				  if (!dbid) {res.status(200).json(-1);return;} // incorrect company

				  db3.modele( session.intr.modeles[ session.intr.c1[company].idModele ].cs );
				  db3.user( session.intr.databases[ session.intr.c1[company].idDB ].cs );

				  require("schematize")(db , schema , db3 )
					.success( function(db){
						db.count( options )
							 .success( function(e){res.status(200).json( e );} )
							 .error(function(){res.status(200).json(-1);});
							})
					.error( function(){res.status(500).json(-1);} );
			  }
			  else {  // no query.count so fetch records
				  if (!session) {res.status(200).json( null);return;}
				  if (typeof company === "undefined") {company = session.intr.c0;} // default company
				  if (!session.intr.c1[company]) {res.status(200).json(null);return;} // incorrect company
				  if (!schema) {res.status(200).json(null);return;} // no schema name given

				  if (!options.where) options.where={};
				  
				  	// fetch a page
				  if ((typeof options.page == "number") && (typeof options.bypage == "number") ) {
				  	options.limit = options.bypage;
				  	options.offset = options.bypage * options.page;
				  	delete options.page;
				  	delete options.bypage;
				  }
				  else if (typeof options.limit !== "number") options.limit = 20;

				  switch(db){
				   case "m":
				   case "modele": db="modele"; break;
				   default :
					 db="user"
					 options.where.company = company;
				  }

				  var dbid = session.intr.c1[company];
				  if (!dbid) {res.status(200).json(null);return;} // incorrect company

				  db3.modele( session.intr.modeles[ session.intr.c1[company].idModele ].cs );
				  db3.user( session.intr.databases[ session.intr.c1[company].idDB ].cs );

				  require("schematize")(db , schema , db3 )
					.success( function(db){
						db.findAll( options )
							 .success( function(Table){res.status(200).json( Table(null, null, null, true) );} )
							 .error(function(){res.status(200).json(null);});
							})
					.error( function(){res.status(500).json(null);} );
			  }
		}
		
		/**** 
			To Update a record :
				POST /d {c:company, s:schema, v:value, k:key}
			To add a record :
				POST /d {c:company, s:schema, v:value}
		****/
	function postData(req, res){
		 var post = JSON.parse(req.body.post),
		 	 company = post.c,
			 schema = post.s,
			 value = post.v,
			 key = post.k,
			 session = uniC(req).get();

		  if (!session) {res.status(200).json(null);return;}
		  if (!company) {company = session.intr.c0;} // default company

		  var dbid = session.intr.c1[company];
		  if (!dbid) {res.status(500).json(null);return;} // incorrect company
		  db3.modele( session.intr.modeles[ session.intr.c1[company].idModele ].cs );
		  db3.user( session.intr.databases[ session.intr.c1[company].idDB ].cs );
		  
		  if (key){ //update
			  if (typeof key=="string") key={id:parseInt(key,10)};
			  updateData(company,schema,key,value,db3).success(function(){res.json(true);}).error(function(){res.json(null);});
		  }
		  else { // append
			  appendData(company,schema,value,db3).success(function(e){res.json(e);}).error(function(){res.json(null);});
		  }
		}

		function updateData(company, schema, key, setValues, db3){
			var where={}, values={}, fields=[],
				onSuccess=function(){}, onError=function(){};

			require("schematize")("user",schema,db3)
				.success( function(Table){
					 Table.fields("primary").forEach(
						function(field){
							where[field]= (typeof key[field] != "undefined") ?  key[field] : null;
						});
					 Table.fields().forEach(
						function(field){
						  if ( Table.fields("primary").indexOf(field)>=0 ) return;
						  if ( typeof setValues[field] == "undefined" ) return;
						  values[field] = setValues[field];
						  fields.push( field );
						});
					 where.company = company;
					 Table.update(values , {where:where} , fields)
						.success( function(e){ onSuccess(e(null,null, null, true)); } )
						.error( function(){ onError(); } );
					});

			return { success:function(e){onSuccess=e;return this;} , error:function(e){onError=e;return this;}};
		}

		function appendData(company,schema, setValues,db3){
			var values={},fields=[],
				onSuccess=function(){},onError=function(){};
			
			require("schematize")("user",schema,db3)
				.success( function(Table){
					 Table.fields().forEach(
						function(field){
						  if ( typeof setValues[field] == "undefined" ) return;
						  values[field] = setValues[field];
						  fields.push( field );
						});
					 values.company = company;
					 Table.create(values, fields)
						.success( function(e){ onSuccess(e(null,null,true)); } )
						.error( function(){ onError(); } );
					});
			return { success:function(e){onSuccess=e;return this;} , error:function(e){onError=e;return this;}};
		}

		/**** 
			Delete a record :
				DELETE /d {c:company, d:db, s:schema, k:key}
		****/
	function deleteData(req,res){
			 var session=uniC(req).get(),
				 post = JSON.parse(req.body.post) || {},
				 company=post.c,
				 db=post.d,
				 schema=post.s,
				 key=post.k || {};

			  if (!session) {res.json(200, null);return;}
			  if (!company) {company = session.intr.c0;} // default company
			  var dbid = session.intr.c1[company];
			  if (!dbid) {res.status(500).json(null);return;} // incorrect company
			  db3.user( session.intr.databases[ session.intr.c1[company].idDB ].cs );

			  destroyData(company,schema,key,db3).success(function(){res.json(true);}).error(function(){res.json(null);});
		}

		function destroyData(company,schema,key,db3){
			var options={where:{}},
				onSuccess=function(){}, onError=function(){};
			
			require("schematize")("user",schema,db3)
				.success( function(Table){
					 Table.fields("primary").forEach(
						function(field){
						 if (typeof key[field] != "undefined") options.where[field]=key[field]; else options.where[field]=null;
						});
					 options.where.company = company;
					 Table.find( options ).then(
					 		function(fRecord,record){
								if (!record) {onError();return;}
								record.destroy().then( onSuccess, onError );
							},
					 		onError
					  );
					 }
					)
			return { success:function(e){onSuccess=e;return this;} , error:function(e){onError=e;return this;}};
		}


	return {getData:getData, postData:postData, deleteData:deleteData}
}
