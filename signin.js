var qamoos,
	path = require("path"),
	ID=require("./ID"),
	MD5=require("./md5"),
	Hash = require("./hash"),
	schematize=require("schematize").config({path:{
			admin:path.join(__dirname,'schemas/')
		}}),
	db3;

	function signin(req,res){
	 var uniC = require("unic")(req);

	 var post= (req.body && req.body.post)||"{}";
	 try {
	 	post = JSON.parse( post );
	 }
	 catch(e) {
	 	post = {};
	 }

	var login=post.lg,password=post.pw,force=post.f,timestamp=post.ts;
	var check = uniC.check(login, force);

	 if (check.err) {res.status(200).json(check);return;}

	 var companyList; // liste des id des companies accessibles à l'utilisateur
	 var usermodules; // liste des id des modules accessibles à l'utilisateur
	 var user;
	 var company;
	 var allDatabases;
	 var allModeles;

	 if (!qamoos) qamoos = require("./brokerMsgs")(req);


	function getSchemas(){
		var then=function(){};

		schematize("admin",['users','companies','userCompanies','userModules','databases','modeles','modules'],db3)
			.error(function(){res.status(200).json(qamoos.getErr('missedData'));})
			.success( function(Tabs){
				tabs = Tabs;
				then();
			} );

		return {then:function(e){then = e; return this;}}
	}


	function getUser( ){ // login -> user
		var where;
		var then = function(){};
		var noReturn = {then : function(){return this;}};

		switch( ID(login) ) {
			case 'login': where={where:{login:login}};break;
			case 'email': where={where:{email:login}};break;
			case 'uid': where={where:{userID:login.slice(1)}};break;
			default: res.status(200).json(qamoos.getErr('badLogin'));return noReturn;
		}

		var check = uniC.check(login, force);
		if (check.err) {res.status(200).json(check);return noReturn;}

	 	tabs.users.find(where)
		 	.success( function(User){
				user=User();
				if (!user) {res.status(200).json(qamoos.getErr('badLogin'));return;}
				tabs.users.update({'lastAttempt':new Date()},{where:{userID:user.userID}},['lastAttempt']);
				if (MD5(user.password+login+timestamp) != password) {res.status(200).json(qamoos.getErr('badPassword'));return;}
				if (user.inactive) {res.status(200).json(qamoos.getErr('inactiveAccount'));return;}
			 	tabs.users.update({'lastLogin':new Date()},{where:{userID:user.userID}},['lastLogin']);
			 	then( user );
			})
			.error( function(){console.log("Error : getUser(",login,") ");} );

	 	return {then : function(e){then = e; return this; }};
	}

	function getCompanies(){
		var then = function(){};

		tabs.companies.find(user.idCompany)
		.success( function(Company){
			 company = Company( null , ["isExpiration","isAlert","isRedemption"]);
			 if (company.isExpiration) {res.status(200).json(qamoos.getErr('expiredAccount'));return;}
			 tabs.userCompanies.findAll({where:{login:user.login}, limit:0})
			 .success( function(userCompanies){
			 	companyList = userCompanies('company').concat(user.idCompany);
			 	then();
				})
			 .error( function(){console.log("error looking for userCompanies");});
			})
		.error( function(){console.log("error looking for company");});
		return {then:function(e){then=e;return this;}};
	}

	function getModules(){
		var then = function(){};

		tabs.userModules.findAll({where:{login:user.login}, limit:0})
		.success( function(userModules){usermodules = userModules("module"); then();})
		.error( function(){console.log("error looking for userModules");});

		return {then:function(e){then=e;return this;}};
	}

	function getDBCredential(){
		var then = function(){};

		tabs.companies.findAll({where:{id:companyList},order:'nom',limit:0}).success( function(Companies){
			 tabs.modeles.findAll({where:{id:Companies('idModele')},limit:0}).success( function(Modeles){
				tabs.databases.findAll({where:{id:Companies('idDB')}, limit:0}).success( function(Databases){
					allDatabases = Databases;
					allModeles = Modeles;
					allCompanies = Companies;
					then();
				}).error( function(){console.log("error looking for Database");});
			}).error( function(){console.log("error looking for Modele");});
		}).error( function(){console.log("error looking for companyList");});
		return {then:function(e){then=e;return this;}};
	}

	 getSchemas().then(
		function(){
			getUser().then(
				function(){
					 getCompanies().then(
					 	function(){
					 		getModules().then(
					 			function(){
					 				getDBCredential().then(
					 					function(){
											var sessionValue = {
													 extr:{lg:user.login , c0:user.idCompany, c1:allCompanies(['nom'],'id'), m:usermodules },
													 intr:{lg:user.login , c0:user.idCompany, c1:allCompanies(['idModele','idDB'],'id'), m:usermodules , databases:allDatabases(['id','cs','server','dbName','user','password'],'id') , modeles:allModeles(['id','cs','server','dbName','user','password'],'id')},
													 ts:new Date().valueOf()
													};
											var logged = uniC.signin(user.login, force, sessionValue );
											if (logged.err) {res.status(200).json( logged ); return;}
											if (company.isAlert) logged.isAlert = true;
											else if (company.isRedemption) logged.isRedemption = true;
											logged.sess = sessionValue.extr;
											res.status(200).json( logged );
					 					});
					 			});
					 	});
					});
		});

}

module.exports = function(db){
	db3 = db;
	return signin;
}	