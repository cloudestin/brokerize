
function ID(obj) {
	if (typeof obj !== "string") return null;
	if (obj.match(/^[a-zA-Z0-9][a-zA-Z0-9\_\-]{4,}$/)) return 'login';
	else if (obj.match(/^@[0-9]+$/)) return 'uid';
	else if (obj.match(/^[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$\b/gi)) return 'email';
	return null;
}


module.exports = ID